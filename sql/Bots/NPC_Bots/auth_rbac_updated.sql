INSERT INTO `rbac_permissions` (`id`, `name`) VALUES 
(1000, 'Command: npcbot'),
(1001, 'Command: npcbot add'),
(1002, 'Command: npcbot remove'),
(1003, 'Command: npcbot reset'),
(1004, 'Command: npcbot command'),
(1005, 'Command: npcbot distance'),
(1006, 'Command: npCbot info'),
(1007, 'Command: npcbot helper'),
(1008, 'Comamnd: npcbot revive'),
(1009, 'Command: maintank');

INSERT INTO `rbac_linked_permissions` (`id`, `linkedId`) VALUES 
(195, 1000),
(195, 1001),
(195, 1002),
(195, 1003),
(195, 1004),
(195, 1005),
(195, 1006),
(195, 1007),
(195, 1008),
(195, 1009);
